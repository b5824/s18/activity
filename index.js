/* 1 */
function add(num1, num2) {
    console.log('Result: ' + (num1 + num2));
}

function subtract(num1, num2) {
    console.log('Result: ' + (num1 - num2));
}
console.log('The displayed sum of 5 and 15');
add(5, 15);
console.log('Displayed difference of 20 and 10');
subtract(20, 10);

/* 
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication. (use return statement)

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division. (use return statement)

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console. */
function multiplication(num1, num2) {
    return num1 * num2;
}

function divide(num1, num2) {
    return num1 / num2;
}

let product = multiplication(50, 10);
let quotient = divide(50, 10);
console.log('The product of 50 and 10:');
console.log(product);
console.log('The quotient of 50 and 10:');
console.log(quotient);

/* 	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-formula: area = pi * (radius ** 2)
			-exponent operator **
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation. (use return statement)

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

		Log the value of the circleArea variable in the console. */

function totalAreaOfCircle(radius) {
    let area = Math.PI * radius ** 2;
    return area;
}

let radius = 15;
let circleArea = totalAreaOfCircle(radius);
console.log(
    'The result of getting the area of a circle with ' + radius + ' radius:'
);
console.log(circleArea);

/* 	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-formula for average: num1 + num2 + num3 + ... divide by total numbers
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation. (use return statement) 

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	
 */

function totalAverage(num1, num2, num3, num4) {
    let average = (num1 + num2 + num3 + num4) / 4;
    return average;
}

let averageVar = totalAverage(20, 40, 60, 80);
console.log('The average of 20, 40, 60, 80');
console.log(averageVar);

/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed. (use return statement)
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
//First solution
function calcPassingScore(score1, score2) {
    let total = score2 * 0.75;
    let result = score1 >= total;
    return result;
}

let isPassingScore = calcPassingScore(38, 50);
console.log('Is 38/50 a passing score?');
console.log(isPassingScore);

//Second solution
function calcScore(score1, score2) {
    let total = (score1 / score2) * 100;
    let result = total >= 75;
    return result;
}

let isPassingScore2 = calcScore(38, 50);
console.log('Is 38/50 a passing score?');
console.log(isPassingScore2);